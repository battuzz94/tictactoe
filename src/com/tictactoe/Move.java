package com.tictactoe;

import java.io.Serializable;

public class Move implements Serializable {
	private int row;
	private int col;
	private Player player;
	
	public Move(int row, int col, Player player) {
		this.row = row;
		this.col = col;
		this.player = player.getCopy();
	}
	public Move(int row, int col) {
		this.row = row;
		this.col = col;
		this.player = null;
	}
	
	public int getRow() {
		return row;
	}
	public int getCol() {
		return col;
	}
	public Player getPlayer() {
		return this.player;
	}
	
	public void setPlayer(Player p) {
		this.player = p;
	}
	
	@Override
	public String toString() {
		return "Mossa. row = " + this.row + " col = " + this.col + " player = " + this.player;
	}
}
