package com.tictactoe;

import java.io.Serializable;

public class MessageNotification implements Serializable {
	public enum MessageContent {
		REQUEST_MOVE("Request move"),			// Server richiede al giocatore quale mossa vuole fare 
		REQUEST_CLIENT_NAME("Request client move"), 	// Il server chiede al client il suo nome
		BOARD_UPDATE("Board update"), 			// Il server notifica tutti i giocatori che c'è stato un aggiornamento
		REQUEST_PLAYERS_NAME("Request players name"),	// I client richiedono al server i nomi di tutti i giocatori
		INVALID_MOVE("Invalid move"), 			// Il server notifica il giocatore che ha fatto una mossa non valida
		SEND_MOVE("Send move"), 				// Il client invia al server la mossa che vuole fare
		SEND_CLIENT_NAME("Send client name"),		// Il client invia al server il suo nome
		GAME_FINISHED("The game is finished"),		// Il gioco è finito e informa i client su chi è il vincitore
		OK("Ok") ;						// Il client oppure il server invia un ack
		
		String description;
		
		private MessageContent(String description) {
			this.description = description;
		}
		@Override
		public String toString() {
			return this.description;
		}
		
	};
	
	private MessageContent content;
	private Serializable additionalInfo;
	
	public MessageNotification(MessageContent content) {
		this(content, new String(""));
	}
	
	public MessageNotification(MessageContent content, Serializable additionalInfo) {
		this.additionalInfo = additionalInfo;
		this.content = content;
	}
	
	
	public MessageContent getContent() {
		return this.content;
	}
	
	public Object getAdditionalInfo() {
		return this.additionalInfo;
	}
	
	@Override
	public String toString() {
		return "MessageNotification Object.\ncontent: " + 
				this.content.toString() + 
				"additionalInfo: " + 
				this.additionalInfo.toString();
	}

}
