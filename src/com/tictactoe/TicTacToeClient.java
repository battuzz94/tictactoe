package com.tictactoe;

import java.io.BufferedReader;


import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class TicTacToeClient {
	
	public static final int SERVER_PORT = 40000;
	
	public static void main(String args[]) {
		print_header();
		
		String hostname = get_hostname();
		System.out.println("Cerco di connettermi al server...");
		Socket server = connect_to_server(hostname);
		System.out.println("Connessione stabilita!");
		
		if (server != null) 
			game_loop(server);
	}
	
	private static void print_header() {
		System.out.println("TicTacToe Client side!");
		System.out.println("Benvenuto!");
		System.out.println("Assicurati di aver avviato il server");
	}
	
	private static String get_hostname() {
		System.out.println("Inserisci l'hostname del server");
		Scanner in = new Scanner(System.in);
		
		String hostname = in.nextLine();
		return hostname;
	}
	
	private static String get_nome() {
		Scanner in = new Scanner(System.in);
		System.out.println("Inserisci il tuo nome: ");
		return in.nextLine();
	}
	
	private static Socket connect_to_server(String hostname) {
		Socket socket;
		try {
			socket = new Socket(hostname, SERVER_PORT);
			// Aspetta una richiesta dal server e poi invia il proprio nome
			
			
		}
		catch(Exception e) {
			System.out.println("Non riesco a connettermi al server. Riprovare piu' tardi");
			e.printStackTrace();
			return null;
		}
		
		return socket;
		
	}
	
	private static void game_loop(Socket server) {
		ObjectInputStream inStream;
		ObjectOutputStream outStream;
		try {
			inStream = new ObjectInputStream(server.getInputStream());
			outStream = new ObjectOutputStream(server.getOutputStream());
			
			
			while (Boolean.TRUE) {
				try {
					Object command = inStream.readObject();
					MessageNotification message = (MessageNotification) command;
					
					MessageNotification request_answer = manage_request(message);

					outStream.writeObject(request_answer);
				}
				catch (ClassCastException e) {
					// Ignora il messaggio e va avanti
					System.out.println("Attenzione, ho ricevuto un pacchetto non riconosciuto.");
					e.printStackTrace();
					continue;
				}
			}
			
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e) {
			System.out.println("Qualcosa è andato storto...");
			e.printStackTrace();
			return;
		}
		
	}
	
	private static MessageNotification manage_request(MessageNotification message) {
		
		switch(message.getContent()) {
			case REQUEST_MOVE:
				Move mossa = get_mossa();
				return new MessageNotification(
						MessageNotification.MessageContent.SEND_MOVE,
						mossa);
			case REQUEST_CLIENT_NAME:
				String nome = get_nome();
				return new MessageNotification(
						MessageNotification.MessageContent.SEND_CLIENT_NAME,
						nome);
			case BOARD_UPDATE:
				Board board = (Board) message.getAdditionalInfo();
				board.print();
				return new MessageNotification(MessageNotification.MessageContent.OK);
			case GAME_FINISHED:
				GameController game = (GameController) message.getAdditionalInfo();
				Player winner = game.getWinner();
				
				if (winner != null) {
					System.out.println("Partita finita! Ha vinto il giocatore: " + winner.getName());
				}
				else {
					System.out.println("La partita è finita con un pareggio!");
				}
				
				return new MessageNotification(MessageNotification.MessageContent.OK);
				
			default:
				return new MessageNotification(
						MessageNotification.MessageContent.OK);
		}
	}
	
	
	private static Move get_mossa() {
		System.out.println("È il tuo turno. Quale mossa vuoi fare? (0-8) ");
		Scanner in = new Scanner(System.in);
		int mossa = in.nextInt();
		
		return new Move(mossa/3, mossa%3);
	}
}






























