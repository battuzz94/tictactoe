package com.tictactoe;

import java.io.Serializable;



import java.util.ArrayList;

/**
 * Questa classe rappresenta una scacchiera di tris, di dimensione n x m, con n
 * e m qualsiasi.
 * 
 */
public class Board implements Serializable {
	/**
	 * Costruisce una nuova scacchiera con un certo numero di righe e di colonne.
	 * @param rows Il numero di righe della scacchiera
	 * @param cols Il numero di colonne della scacchiera
	 */
	public Board(int rows, int cols) {
		this.rows = rows;
		this.cols = cols;
		this.board = new ArrayList<ArrayList<Integer>>(rows);
		// Inizializza la scacchiera
		for (int i = 0; i < rows; i++) {
			this.board.add(new ArrayList<Integer>(cols));
			for (int j = 0; j < cols; j++)
				this.board.get(i).add(-1);
		}
		
	}
	
	/**
	 * Costruisce una nuova scacchiera, copiata da quella passata per parametro
	 * @param b la scacchiera da copiare.
	 */
	public Board(Board b) {
		this.rows = b.rows;
		this.cols = b.cols;
		this.board = new ArrayList<ArrayList<Integer>>(rows);
		for (int i = 0; i < rows; i++) {
			this.board.add(new ArrayList<Integer>(cols));
			for (int j = 0; j < cols; j++)
				this.board.get(i).add(b.board.get(i).get(j));
		}
	}
	
	/**
	 * Controlla se la mossa m è una mossa valida per lo stato corrente della scacchiera
	 * @param m la mossa da controllare
	 * @return true se è possibile fare quella mossa, false altrimenti
	 */
	public boolean isValidMove(Move m) {
		int move_row = m.getRow();
		int move_col = m.getCol();
		
		return (move_row >= 0 && move_row < this.rows) &&
				(move_col >= 0 && move_col < this.cols) &&
				board.get(move_row).get(move_col) == -1;
	}
	
	/**
	 * Applica la mossa sulla scacchiera. La mossa deve essere una mossa valida.
	 * @param m La mossa da applicare
	 */
	public void setMove(Move m) {
		assert(isValidMove(m));
		board.get(m.getRow()).set(m.getCol(), m.getPlayer().getId());
	}
	
	/**
	 * Controlla ogni riga per vedere se il giocatore p ha fatto un tris
	 * @param p Il giocatore per cui si vuole controllare se ha fatto tris
	 * @return true se il giocatore ha fatto tris su una riga, false altrimenti
	 */
	public boolean checkRow(Player p) {
		boolean found = false;
		for (int r = 0; r < rows && !found; r++) {
			found = true;
			for (int c = 0; c < cols; c++) {
				if (board.get(r).get(c) != p.getId())
					found = false;
			}
		}
		return found;
	}
	
	/**
	 * Controlla tutte le colonne per vedere se il giocatore p ha fatto tris
	 * @param p Il giocatore per cui si vuole controllare le colonne
	 * @return true se il giocatore p ha fatto tris su una colonna, false altrimenti
	 */
	public boolean checkCol(Player p) {
		boolean found = false;
		for (int c = 0; c < cols && !found; c++) {
			found = true;
			for (int r = 0; r < rows; r++) {
				if (board.get(r).get(c) != p.getId())
					found = false;
			}
		}
		return found;
	}
	
	/**
	 * Controlla su entrambe le diagonali per vedere se c'è stato un tris
	 * @param p Il giocatore per cui si vuole controllare se ha fatto tris
	 * @return true se il giocatore p ha fatto tris, false altrimenti.
	 */
	public boolean checkDiagonal(Player p) {
		boolean found = true;
		
		for (int i = 0; i < rows; i++) {
			if (board.get(i).get(i) != p.getId())
				found = false;
		}
		if (!found) {
			found = true;
			for (int i = 0; i < rows; i++) {
				if (board.get(i).get(rows-i-1) != p.getId())
					found = false;
			}
		}
		return found;
	}
	
	/**
	 * Stampa a video la scacchiera
	 */
	public void print() {
		System.out.println();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				char symbol;
				if (board.get(i).get(j) == -1)
					symbol = ' ';
				else
					symbol = SYMBOLS[board.get(i).get(j)];
				
				System.out.printf(" %c %c", symbol, j != cols-1 ? '|' : ' ');
			}
			System.out.println();
			if (i != rows-1)
				for (int j = 0; j < cols; j++)
					System.out.print("----");
			
			System.out.println();
		}
	}
	
	/**
	 * Restituisce una stringa contenente la descrizione e lo stato della scacchiera
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String s =  "Board Object.\nrows: " + rows + "\ncols: " + cols + "\nmatrix values: ";
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) 
				s += " " + this.board.get(i).get(j) + " ";
			s+="\n";
		}
		return s;
	}
	
	/**
	 * @deprecated
	 * Crea una stringa con tutti i valori della scacchiera in serie 
	 * @return Una stringa contenente tutti i valori della scacchiera uno dopo l'altro
	 */
	public String serialize() {
		String s = "";
		for (ArrayList<Integer> row : board) {
			for (Integer val : row) {
				s += val + " ";
			}
		}
		return s;
	}
	
	/**
	 * Crea una copia della scacchiera
	 * @return Una copia della scacchiera
	 */
	public Board getCopy() {
		Board b = new Board(this.rows, this.cols);
		b.board = new ArrayList<ArrayList<Integer>>(this.rows);
		for (int i = 0; i < this.rows; i++) {
			b.board.add(new ArrayList<Integer>(this.cols));
			for (int j = 0; j < this.cols; j++)
				b.board.get(i).add(-1);
		}
		return b;
	}
	
	/**
	 * Verifica se la scacchiera è piena
	 * @return <code>true</code> se la scacchiera è piena, <code>false</code> altrimenti
	 */
	public boolean isFull() {
		for (ArrayList<Integer> row : board) {
			for (Integer val : row)
				if (val == -1) return false;
		}
		return true;
	}
	
	/**
	 * Ritorna il numero di righe della scacchiera
	 * @return il numero di righe della scacchiera
	 */
	public int getRows() {
		return rows;
	}
	
	/**
	 * Ritorna il numero di colonne della scacchiera
	 * @return Il numero di colonne della scacchiera
	 */
	public int getCols() {
		return cols;
	}
	
	/** 
	 * Restituisce un riferimento alla scacchiera
	 * @return Un riferimento alla scacchiera
	 */
	public ArrayList<ArrayList<Integer>> getBoard() {
		return board;
	}
	/**
	 * Compara due scacchiere e dice se sono uguali
	 * @param aBoard La scacchiera da comparare
	 * @return 0 se le scacchiere sono uguali, -1 se la prima è più piccola (meno caselle), 1 se la prima è più grande oppure uguale ma con valori diversi
	 */
	public int equals(Board aBoard) {
		// La prima è più piccola della seconda
		if (this.cols * this.rows < aBoard.cols * aBoard.rows)
			return -1;
		// Le scacchiere hanno dimensione diversa
		if (this.cols != aBoard.cols || this.rows != aBoard.rows)
			return 1;
		
		//Confronta i valori della scacchiera
		boolean equal = true;
		for (int i = 0; i < this.rows && equal; i++) {
			for (int j = 0; j < this.cols && equal; j++) {
				if (this.board.get(i).get(j) != aBoard.board.get(i).get(j)) 
					equal = false;
			}
		}
		if (equal)
			return 0;
		return 1;
	}

	/**
	 * Questa classe interna rappresenta i diversi simboli che vengono dati ai giocatori
	 * NOTA: Il numero massimo di giocatori è definito qui ed è 8.
	 */
	private enum Symbol {
		PLAYER_1("X"),
		PLAYER_2("O"),
		PLAYER_3("#"),
		PLAYER_4("&"),
		PLAYER_5("+"),
		PLAYER_6("="),
		PLAYER_7("-"),
		PLAYER_8("*"),
		EMPTY(" ");
		private static final String symbol_string = "XO#&+=-* ";
		
		private Symbol(String symbol) {
			this.symbol = symbol;
		}
		
		/**
		 * Ritorna il simbolo corrispondente all'identificatore (0-based) di player.
		 * @param player Identificativo del giocatore (0-based)
		 * @return Carattere corrispondente a player, oppure <code>'?'</code> altrimenti.
		 */
		public String getValueOfPlayer(int player) {
			if (player >= 0 && player < 9)
				return Character.toString(symbol_string.charAt(player));
			else
				return "?";
		}
		
		private String symbol; 
	}
	
	
	
	/*
	 * VARIABILI DI STATO E COSTANTI
	 */
	
	/**
	 * Costante che rappresenta l'id per la serializzazione
	 */
	private static final long serialVersionUID = 18734789273487987L;
	
	/** 
	 * Lista di simboli usati per stampare a video le pedine di ogni giocatore 
	 */
	private static final char[] SYMBOLS = {'X', 'O', '#', '+', '=', '&'}; 
	
	/**
	 * Numero di righe della scacchiera
	 */
	int rows;
	
	/**
	 * Numero di colonne della scacchiera
	 */
	int cols;
	
	/** 
	 * La matrice che rappresenta la scacchiera 
	 */
	ArrayList< ArrayList< Integer > > board;   
}
