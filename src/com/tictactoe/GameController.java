package com.tictactoe;

import java.io.Serializable;

/**
 * Gestisce una partita nel suo complesso.
 * @author Andrea
 * 
 */
public class GameController implements Serializable {
	/**
	 * La scacchiera associata a questa partita
	 */
	Board board;
	
	/**
	 * Lista di giocatori che partecipano alla partita
	 */
	Player[] players;
	
	/**
	 * Indica l'indice del giocatore che deve fare la sua mossa.
	 */
	int current_turn;
	
	/**
	 * Variabile che vale <code>true</code> se il gioco è terminato, <code>false</code> altrimenti.
	 * NOTA: viene aggiornata solo dopo una chiamata a <code> finished() </code>
	 */
	boolean game_finished;
	
	/**
	 * Il giocatore che ha vinto la partita, oppure null se la partita non è ancora finita oppure è finita in pareggio.
	 * NOTA: viene aggiornata solo dopo una chiamata a <code> finished() </code>
	 */
	Player winner = null;
	
	
	/**
	 * Costruisce una nuova partita iniziandola da una scacchiera definita
	 * @param board la scacchiera da cui voler cominciare.
	 * @param players La lista di giocatori che partecipano alla partita.
	 */
	public GameController(Board board, Player[] players) {
		this.board = board;
		this.players = players;
		this.current_turn = 0;       // Il turno iniziale è del giocatore 0
		this.game_finished = false;
	}
	/**
	 * Costruisce una nuova partita iniziando con una scacchiera standard 3x3
	 * @param players La lista di giocatori che partecipano alla partita
	 */
	public GameController(Player[] players) {
		this(new Board(3, 3), players);
	}
	
	/**
	 * Controlla se la partita è terminata oppure no.
	 * @return <code> true </code> se la partita è terminata, oppure <code> false </code> altrimenti.
	 */
	public boolean finished() {
		for (Player p : players) {
			if (board.checkRow(p) || board.checkCol(p) || board.checkDiagonal(p)) {
				game_finished = true;
				winner = p;
			}
		}
		return game_finished || board.isFull();
	}
	
	/**
	 * Restituisce il giocatore corrente
	 * @return Il giocatore che deve fare la sua mossa.
	 */
	public Player currentPlayer() {
		return players[current_turn];
	}
	
	/**
	 * Restituisce il vincitore della partita, oppure <code> null </code> altrimenti.
	 * @return Il giocatore che ha vinto la partita, oppure <code> null </code> se è stato un pareggio.
	 */
	public Player getWinner() {
		if (game_finished) 
			return winner;
		else
			return null;
	}
	
	/**
	 * Fa avanzare il gioco di un turno.
	 */
	public void nextTurn() {
		current_turn = (current_turn + 1) % (players.length);
	}
	
	
}
