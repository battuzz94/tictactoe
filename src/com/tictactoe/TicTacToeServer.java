package com.tictactoe;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.regex.Pattern;

public class TicTacToeServer {
	
	public static final int BOARD_ROWS = 3;
	public static final int BOARD_COLS = 3;
	
	
	public static void main(String[] args) {
		Player[] players;
		
		// Inizializza i giocatori
		int number_players = getNumberPlayers();
		players = new Player[number_players];
		
		Scanner in = new Scanner(System.in);
		try {
			ServerSocket server = new ServerSocket(40000);
		
			for (int i = 0; i < players.length; i++) {
				System.out.println("In attesa che il giocatore " + i + " si connetta..");
				Socket clientSocket = server.accept();
				System.out.println("Il giocatore " + i + " ha appena richiesto una connessione.");
				players[i] = new Player(i);
				players[i].setSocket(clientSocket);
				
				try {
					players[i].beginConnection();
				}
				catch (Exception e) {
					System.out.println("Purtroppo non riesco a connettermi con il giocatore " + i);
					e.printStackTrace();
					i--;
					continue;
				}
				System.out.println("Il giocatore " + players[i].getName() + " si è appena connesso!");
			}
		}
		
		catch (Exception e) {
			System.out.println("Non riesco a stabilire nessuna connessione..");
			e.printStackTrace();
			return;
		}
		
		
		print_menu();
		
		while(Boolean.TRUE) {
			Board board = new Board(BOARD_ROWS, BOARD_COLS);
			GameController game = new GameController(board, players);
			while (!game.finished()) {
				Player current_player = game.currentPlayer();
				System.out.println("È il turno di " + current_player.getName());
				board.print();
				for (Player p : players) {
					p.notifyBoardUpdate(board);
				}
				try {
					Move m = current_player.getMove();
				
					while (!board.isValidMove(m)) {
						current_player.notifyBadMove();
						m = current_player.getMove();
					}
					board.setMove(m);
					game.nextTurn();
				}
				catch (Exception e) {
					e.printStackTrace();
					break;
				}
				
			}
			
			board.print();
			for (Player p : players)
				p.notifyGameFinished(game);
			
			System.out.println("Gioco finito.");
			if (game.getWinner() != null) {
				System.out.println("Ha vinto: " + game.getWinner().getName());
			}
			else {
				System.out.print("La partita è finita in un pareggio!");
			}
			System.out.println("Vuoi fare un'altra partita? (Y/N)");
			String ans = in.nextLine();
			while (!(ans.toLowerCase().equals("y") || ans.toLowerCase().equals("n"))){
				System.out.println("Non ho capito.. Vuoi fare un'altra partita? (Y/N)");
				ans = in.nextLine();
			}
			if (ans.toLowerCase().equals("n")) {
				break;
			}
		}
		
	}
	
	public static void print_menu() {
		System.out.println("Tutti i giocatori si sono connessi. Il gioco comincia..");
		System.out.println("TicTacToe Server side.");
		System.out.println("Ora comincia la partita!");
	}
	
	public static int getNumberPlayers() {
		Scanner in = new Scanner(System.in);
		System.out.print("Quanti giocatori ci saranno? ");
		int num = in.nextInt();
		in.nextLine();
		return num;
	}
	
	public static void manage_connection() {
		
	}
}
