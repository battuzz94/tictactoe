package com.tictactoe;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class MessageNotificationTest {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Inserisci 0 per il server o 1 per il client");
		int choice = in.nextInt();
		assert (choice == 0 || choice == 1);
		
		
		if (choice == 0) {
			//Codice server
			try {
				ServerSocket s = new ServerSocket(30800);
				Socket client = s.accept();
				System.out.println("Ho accettato la richiesta del client");
				
				ObjectOutputStream os = new ObjectOutputStream(client.getOutputStream());
				
				System.out.println("Ho aperto il flusso di oggetti");
				// Invia un oggetto di tipo Board al client
				Board board = new Board(3, 3);
				board.setMove(new Move(2, 1, new Player(0)));
				
				
				MessageNotification mn = new MessageNotification(MessageNotification.MessageContent.BOARD_UPDATE, board);
				System.out.println("notifica che sto mandando: " + mn);
				os.writeObject(mn);
			}
			catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
			
		}
		else {
			//Codice client
			try {
				Socket s = new Socket("127.0.0.1", 30800);
				System.out.println("Mi sono connesso al server");
				ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
				System.out.println("Ho aperto il flusso di oggetti");
				Object a = ois.readObject();
				MessageNotification ans = (MessageNotification) a;
				Board ob = (Board) ans.getAdditionalInfo();
				
				System.out.println("Ho ricevuto la notifica: " + ans);
				System.out.println("board: " + ob);
				ois.close();
				
			}
			catch(Exception e) {
				e.printStackTrace();
				System.exit(2);
			}
		}
		
		
	}
}
