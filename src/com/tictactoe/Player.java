package com.tictactoe;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.Scanner;

public class Player implements Serializable {
	String nome;
	int id;
	
	// Queste variabili non devono essere serializzate, perciò vanno definite 'transient'.
	transient Socket socket;
	transient ObjectOutputStream out;
	transient ObjectInputStream in;
	
	public Player(String nome, int id) {
		this(nome);
		this.id = id;
	}
	
	public Player(String nome) {
		this.nome = nome;
	}
	
	public Player(int id) {
		this.nome = "";
		this.id = id;
	}
	
	public void setSocket(Socket s) throws IOException {
		this.socket = s;
		out = new ObjectOutputStream(this.socket.getOutputStream());
		in = new ObjectInputStream(this.socket.getInputStream());
	}
	
	public Player getCopy() {
		return new Player(this.nome, this.id);
	}
	
	public void setPlayerId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	/*
	 * Chiede al giocatore di scrivere la propria mossa
	 */
	
	public Move getMove() throws IOException, ClassNotFoundException {
		out.writeObject(new MessageNotification(
				MessageNotification.MessageContent.REQUEST_MOVE
				));
		
		Object answer = in.readObject();
		MessageNotification notification;
		try {
			notification = (MessageNotification) answer;
		}
		catch (ClassCastException e) {
			System.out.println("Ho ricevuto un oggetto sconosciuto..");
			throw new IOException();
		}
		
		assert(notification.getContent() == MessageNotification.MessageContent.SEND_MOVE);
		
		// @TODO CRITICAL
		Move m = (Move) notification.getAdditionalInfo();
		m.setPlayer(this);
		
		return m;
		
	}
	
	public void notifyBadMove() {
		//@TODO
		System.out.println(this.nome + " hai fatto una mossa non valida!");
	}
	
	public void notifyBoardUpdate(Board board) {
		try {
			MessageNotification mg = new MessageNotification(MessageNotification.MessageContent.BOARD_UPDATE, board);
			out.writeObject(mg);
			out.reset();
			Object answer = in.readObject();
			assert(answer instanceof MessageNotification);
			assert(((MessageNotification) answer).getContent() == MessageNotification.MessageContent.OK);
		}
		catch (Exception e) {
			System.out.println("C'è stato un errore nel notificare lo stato della scacchiera!");
			e.printStackTrace();
		}
	}
	
	public void beginConnection() {
		try {
			out.writeObject(new MessageNotification(
					MessageNotification.MessageContent.REQUEST_CLIENT_NAME
					));
			Object answer = in.readObject();
			assert(answer instanceof MessageNotification);
			
			MessageNotification notification = (MessageNotification) answer;
			assert (notification.getAdditionalInfo() instanceof String);
			
			this.nome = (String) notification.getAdditionalInfo();
			
		}
		catch (Exception e) {
			e.printStackTrace();
			this.nome = "INVALID";
		}
	}
	
	public String getName() {
		//@TODO
		return this.nome;
	}
	
	@Override
	public String toString() {
		return "Giocatore: nome: " + this.nome + " id: " + this.id;
	}
	
	
	public void notifyGameFinished(GameController game) {
		try {
			out.writeObject(new MessageNotification(
					MessageNotification.MessageContent.GAME_FINISHED,
					game));
			out.reset();
			MessageNotification ans = (MessageNotification) in.readObject();
			
			assert (ans.getContent() == MessageNotification.MessageContent.OK);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
}
