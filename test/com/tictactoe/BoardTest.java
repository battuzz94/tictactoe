/**
 * 
 */
package com.tictactoe;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Andrea
 *
 */
public class BoardTest {

	/**
	 * Test method for {@link com.tictactoe.Board#Board(int, int)}.
	 */
	@Test
	public void testBoardIntInt() {
		int n_rows = 3;
		int n_cols = 7;
		Board b = new Board(n_rows, n_cols);
		assertNotNull(b);
		assertEquals(b.getRows(), n_rows);
		assertEquals(b.getCols(), n_cols);
	}

	/**
	 * Test method for {@link com.tictactoe.Board#Board(com.tictactoe.Board)}.
	 */
	@Test
	public void testBoardBoard() {
		Board board = new Board(3, 5);
		board.setMove(new Move(1, 1, new Player(1)));
		Board board2 = new Board(board);
		assertNotSame(board, board2);
		assertTrue(board.equals(board2) == true);
	}

	/**
	 * Test method for {@link com.tictactoe.Board#isValidMove(com.tictactoe.Move)}.
	 */
	@Test
	public void testIsValidMove() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.tictactoe.Board#setMove(com.tictactoe.Move)}.
	 */
	@Test
	public void testSetMove() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.tictactoe.Board#checkRow(com.tictactoe.Player)}.
	 */
	@Test
	public void testCheckRow() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.tictactoe.Board#checkCol(com.tictactoe.Player)}.
	 */
	@Test
	public void testCheckCol() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.tictactoe.Board#checkDiagonal(com.tictactoe.Player)}.
	 */
	@Test
	public void testCheckDiagonal() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.tictactoe.Board#print()}.
	 */
	@Test
	public void testPrint() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.tictactoe.Board#toString()}.
	 */
	@Test
	public void testToString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.tictactoe.Board#serialize()}.
	 */
	@Test
	public void testSerialize() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.tictactoe.Board#getCopy()}.
	 */
	@Test
	public void testGetCopy() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.tictactoe.Board#isFull()}.
	 */
	@Test
	public void testIsFull() {
		fail("Not yet implemented");
	}

}
